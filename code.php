<?php
	function getLetterGrade($letterEquivalent){
		if($letterEquivalent >= 98 && $letterEquivalent <= 100) {
			return $letterEquivalent . ' is equivalent to A+'; 
		} 
		else if ($letterEquivalent >= 95 && $letterEquivalent <= 97) {
			return $letterEquivalent . ' is equivalent to A';
		} 
		else if ($letterEquivalent >= 92 && $letterEquivalent <= 94) {
			return $letterEquivalent . ' is equivalent to A-';
		} 
		else if ($letterEquivalent >= 89 && $letterEquivalent <= 91) {
			return $letterEquivalent . ' is equivalent to B+';
		} 
		else if ($letterEquivalent >= 86 && $letterEquivalent <= 88) {
			return $letterEquivalent . ' is equivalent to B';
		} 
		else if ($letterEquivalent >= 83 && $letterEquivalent <= 85) {
			return $letterEquivalent . ' is equivalent to B-' ;
		} 
		else if ($letterEquivalent >= 80 && $letterEquivalent <= 82) {
			return $letterEquivalent . ' is equivalent to C+';
		} 
		else if ($letterEquivalent >= 77 && $letterEquivalent <= 79) {
			return $letterEquivalent . ' is equivalent to C';
		} 
		else if ($letterEquivalent >= 75 && $letterEquivalent <= 76) {
			return $letterEquivalent . ' is equivalent to C-';
		} 
		else if ($letterEquivalent <= 75) {
			return $letterEquivalent . ' is equivalent to D';
		} else {
			return 'Kindly check if the value you placed is an integer.';
		}
		
	}

?>