<?php require_once "./code.php"?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>S1: PHP Basics & Selection Control Structures</title>
</head>
<body>
	<h2>Letter Grade Activity</h2>
	<p><?php echo getLetterGrade(87)?></p>
	<p><?php echo getLetterGrade(94)?></p>
	<p><?php echo getLetterGrade(74)?></p>
	<p><?php echo getLetterGrade('string')?></p>
	
</body>
</html>